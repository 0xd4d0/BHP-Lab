#!/usr/bin/env python

from scapy.all import *

# our packet callback
def packet_callback(packet):
	print packet.show()

	if packet[TCP].payload:

		mail_packet = str(packet[TCP].payload)

		if "user" in mail_packet.lower() or "pass" in mail_packet.lower()

			print "[*] Server: %s" % packet[IP].dst
			print "[*] %s" % packet[TCP].payload

# fire up our sniffer
sniff(prn=packet_callback,count=1)
